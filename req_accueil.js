"use strict";

var fs = require("fs");
require('remedial');

var trait = function (req, res, query) {

	var marqueurs;
	var pseudo;
	var password;
	var page;
	var membre;
	var contenu_fichier;
	var listeMembres;
	var i;
	var trouve;
	var listeConnectes;
	var connectes;
	var absent;
	var joueur;
	var libres;
	var l;
	var bank;

	// ON LIT LES COMPTES EXISTANTS

	contenu_fichier = fs.readFileSync("membres.json", 'utf-8');    
	listeMembres = JSON.parse(contenu_fichier);
	var joueur = JSON.parse(fs.readFileSync(query.pseudo + ".json", "utf-8"));

	// ON VERIFIE QUE LE PSEUDO/PASSWORD EXISTE

	trouve = false;
	i = 0;
	while(i<listeMembres.length && trouve === false) {
		if(listeMembres[i].pseudo === query.pseudo) {
			if(listeMembres[i].password === query.password) {
				trouve = true;
			}
		}
		i++;
	}

	// AJOUT DANS FICHIER DES CONNECTES SAUF SI IL EXISTE DEJA

	if(trouve === true) {
		contenu_fichier = fs.readFileSync("connectes.json", 'utf-8');
		listeConnectes = JSON.parse(contenu_fichier);

		absent = true;

		for(i = 0; i < listeConnectes.length; i++) {
			if(listeConnectes[i].joueur === query.pseudo){
				absent = false;
			}
		}

	

		for(i=0; i<listeConnectes.length; i++) {
			if(listeConnectes[i].joueur === query.pseudo){
				listeConnectes[i].etat = "libre";
				listeConnectes[i].adv = "";
				listeConnectes[i].dollar = joueur.dollar;
			}
		}

		if (absent === true) {
			connectes = {};
			connectes.joueur = query.pseudo;
			connectes.etat = "libre";
			connectes.adv = "";

			listeConnectes.push(connectes);
		}

		contenu_fichier = JSON.stringify(listeConnectes);
		fs.writeFileSync("connectes.json", contenu_fichier, 'utf-8');
	}



	// ON RENVOIT UNE PAGE HTML 

	if(trouve === false) {
		// SI IDENTIFICATION INCORRECTE, ON REAFFICHE PAGE ACCUEIL AVEC ERREUR

		page = fs.readFileSync('modele_accueil.html', 'utf-8');

		marqueurs = {};
		marqueurs.erreur = "ERREUR : compte ou mot de passe incorrect";
		marqueurs.liste = " ";
		marqueurs.pseudo = query.pseudo;
		page = page.supplant(marqueurs);
	//} else if(trouve === false && listeMembres[i].pseudo === query.pseudo) {
	
		//page = fs.readFileSync('modele_accueil_deco.html','utf-8');
	} else {
		// SI IDENTIFICATION OK, ON ENVOIE PAGE ACCUEIL MEMBRE

		page = fs.readFileSync('modele_accueil_membre.html', 'UTF-8');
		marqueurs = {};
		marqueurs.dollar = joueur.dollar;
		marqueurs.liste = " ";
		page = page.supplant(marqueurs);
		// ON AFFICHE QUE LES LIBRES SUR LA PAGE ACCUEIL MEMBRE

		for(l=0; l<listeConnectes.length; l++){
			if(listeConnectes[l].etat === "libre" && listeConnectes[l].joueur !== query.pseudo){
				marqueurs.joueur = listeConnectes[l].joueur;

			}
		}
		
		marqueurs = {};
	bank = fs.readFileSync('bank.json', 'UTF-8');
	bank = JSON.parse(bank);
	marqueurs.bank = bank.dollar;
		marqueurs.pseudo = query.pseudo;
		marqueurs.password = query.password;
		page = page.supplant(marqueurs);
	}

	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write(page);
	res.end();
};

//---------------------------------------------------------------------------

module.exports = trait;
