"use strict";

var fs = require('fs');
require('remedial');

var trait = function (res, req, query) {
var page;
var contenu;
var contenu1;
var marqueurs = {};
var adv;
var i;

page = fs.readFileSync("modele_jeu_multi_passif.html","utf-8");
marqueurs = {};
marqueurs.pseudo = query.pseudo;
marqueurs.password = query.password;
marqueurs.adv = query.adv;
page = page.supplant(marqueurs);
contenu = fs.readFileSync("connectes.json","UTF-8");
connectes = JSON.parse(contenu);
contenu1 = fs.readFileSync(query.adv + ".json","UTF-8");
adv = JSON.parse(contenu1);

for(i=0; i < connectes.length; i++) {
	if (connectes[i].joueur === query.adv && adv.lancerJ >= 3) {
		connectes[i].etat = "multi1";
		page = fs.readFileSync("modele_jeu_multi_actif.html","utf-8");
		marqueurs = {};
		marqueurs.pseudo = query.pseudo;
		marqueurs.password = query.password;
		marqueurs.adv = query.adv;
		page = page.supplant(marqueurs);
	}
}
contenu = JSON.stringify(connectes);
fs.writeFileSync("connectes.json", contenu, "utf-8");

contenu1 = JSON.stringify(adv);
fs.writeFileSync(query.adv + ".json", contenu1, "utf-8");

res.writeHead(200, {'Content-Type': 'text/html'});
res.write(page);
res.end();
};

module.exports = trait;
		
