"use strict";

var http = require("http");
var url = require("url");
var querystring = require("querystring");

//-------------------------------------------------------------------------
//// DECLARATION DES DIFFERENTS MODULES CORRESPONDANT A CHAQUE ACTION
////-------------------------------------------------------------------------
//
var req_commencer = require("./req_commencer.js");
var req_formulaire_inscription = require("./req_formulaire_inscription.js");
var req_inscrire = require("./req_inscrire.js");
var req_accueil = require("./req_accueil.js");
var req_accueil_membre = require("./req_accueil_membre.js");
var req_static = require("./req_static.js");
var req_erreur = require("./req_erreur.js");
var req_ordi_lancer = require("./req_ordi_lancer.js");
var req_solo_fin_joueur = require("./req_solo_fin_joueur.js");
var req_solo_lancer = require("./req_solo_lancer.js");
var req_jeu_solo = require("./req_jeu_solo.js");
var req_classement = require("./req_classement.js");
var req_regles = require("./req_regles.js");
var req_deconnecter = require("./req_deconnecter.js");
var req_defi = require("./req_defi.js");
var req_refresh = require("./req_refresh.js");
var req_refresh_reponse = require("./req_refresh_reponse.js");
var req_jeu_multi_actif = require("./req_jeu_multi_actif.js");
var req_refresh_demande = require("./req_refresh_demande.js");
var req_annuler_defi = require("./req_annuler_defi.js");
var req_multi_lancer = require("./req_multi_lancer.js");
var req_jeu_multi_passif = require("./req_jeu_multi_passif.js");
var req_multi_fin_joueur = require("./req_multi_fin_joueur.js");
var req_attendre_jouer = require("./req_attendre_jouer.js");
////-------------------------------------------------------------------------
//// FONCTION DE CALLBACK APPELLEE POUR CHAQUE REQUETE
////-------------------------------------------------------------------------

var traite_requete = function (req, res) {

  	var ressource;
    var requete;
    var pathname;
  	var query;

console.log("URL reçue : " + req.url);
requete = url.parse(req.url, true);
pathname = requete.pathname;
query = requete.query;
console.log("pathname : " + pathname);
console.log("query string (pseudo) : " + query.pseudo);
console.log("query string (password) : " + query.password);

// ROUTEUR

try {
	switch (pathname) {
		case '/':
		case '/req_commencer':
			req_commencer(req, res, query);
			break;
		case '/req_formulaire_inscription':
			req_formulaire_inscription(req, res, query);
			break;
		case '/req_inscrire':
			req_inscrire(req, res, query);
			break;
		case '/req_accueil':
			req_accueil(req, res, query);
			break;
		case '/req_jeu_solo':
			req_jeu_solo(req, res, query);
			break;
		case '/req_solo_lancer':
			req_solo_lancer(req, res, query);
			break;
		case '/req_multi_lancer':
			req_multi_lancer(req, res, query);
			break;
		case '/req_classement':
			req_classement(req, res, query);
			break;
		case '/req_regles':
			req_regles(req, res, query);
			break;
		case '/req_deconnecter':
			req_deconnecter(req, res, query);
			break;
		case '/req_refresh':
			req_refresh(req, res, query);
			break;
		case '/req_refresh_reponse':
			req_refresh_reponse(req, res, query);
			break;
		case '/req_jeu_multi_actif':
			req_jeu_multi_actif(req, res, query);
			break;
		case '/req_refresh_demande':
			req_refresh_demande(req, res, query);
			break;
		case '/req_annuler_defi':
			req_annuler_defi(req, res, query);
			break;
		case '/req_ordi_lancer':
			req_ordi_lancer(req, res, query);
			break;
		case '/req_solo_fin_joueur':
			req_solo_fin_joueur(req, res, query);
			break;
		case '/req_defi':
			req_defi(req, res, query);
			break;
		case '/req_accueil_membre':
			req_accueil_membre(req, res, query);
			break;
		case '/req_jeu_multi_passif':
			req_jeu_multi_passif(req, res, query);
			break;
		case '/req_multi_fin_joueur':
			req_multi_fin_joueur(req, res, query);
			break;
		case '/req_attendre_jouer':
			req_attendre_jouer(req, res, query);
			break;
		default:
			req_static(req, res, pathname);
			break;
	}
} catch (e) {
	console.log('Erreur : ' + e.stack);
	console.log('Erreur : ' + e.message);
	console.trace();
	req_erreur(req, res, query);
}
};

//-------------------------------------------------------------------------
// CREATION ET LANCEMENT DU SERVEUR
//-------------------------------------------------------------------------

var mon_serveur = http.createServer(traite_requete);
var port = 5000;
console.log("Serveur en ecoute sur port " + port);
mon_serveur.listen(port)
