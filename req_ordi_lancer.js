"use strict";

var fs = require("fs");
require('remedial');
var marqueurs;
var trait = function (req, res, query) {
	var contenu;
	var i;
	var dicesIa = [];
	var contenu_fichier;
	var listeMembres;
	var marqueurs;
	var page;
	var isYams;
	var isBigSuite;
	var isLittleSuite;
	var sortedDices;
	var value;
	var number;
	var isFull;
	var hitCounter;
	var isEnoughtDicesEquals;
	var hitsFound;
	var figure;
	var dices;
	var moredicesIa;
	var totalJ;
	var totalIa;
	var p;
	var hdices;
	var trypair;
	var firstpair;
	var secondpair;
	var bank;

	marqueurs = {};
	marqueurs.figure = "Chance";
	marqueurs.resultat = "vous avez perdu";


	var joueur = JSON.parse(fs.readFileSync(query.pseudo + ".json", "utf-8"));
	contenu_fichier = fs.readFileSync("membres.json", "utf-8");
	listeMembres = JSON.parse(contenu_fichier);
	bank = JSON.parse(fs.readFileSync("bank.json", "utf-8"));
	page = fs.readFileSync('modele_jeu_solo_passif.html', 'UTF-8');
	//var connectes = JSON.parse(fs.readFileSync("connectes.json", "utf-8"));

	// Condition pour que l'ordinateur lancer les des.

	for (i = 0; i < joueur.dicesIa.length; i++) {
		if (joueur.figureIa != "FULL" || joueur.figureJ === "YAMS" || joueur.figureJ === "GRANDE SUITE" || joueur.figureJ === "PETITE SUITE") {
			if (joueur.figureIa != "GRANDE SUITE" || joueur.figureJ === "YAMS") {
				if (joueur.figureIa != "PETITE SUITE" || joueur.figureJ === "YAMS" || joueur.figureJ === "GRANDE SUITE") {


					if (joueur.dicesIa[i] != joueur.hdices || joueur.lanceria === 0) {
						if (joueur.dicesIa[i] != joueur.firstpair && joueur.dicesIa[i] != joueur.secondpair || joueur.figureJ === "GRANDE SUITE" || joueur.figureJ === "PETITE SUITE" || joueur.figureJ === "YAMS" || joueur.lanceria === 0) {
							joueur.dicesIa[i] = Math.floor(Math.random() * 6) + 1;
						}
					}
				}
			}
		}
	}
	dices = joueur.dicesIa;
	trydice(dices);
	joueur.hdices = hdices;
	trypair(dices);
	joueur.firstpair = firstpair;
	joueur.secondpair = secondpair;

	if (isYams(dices) === true) {
		console.log("YAMS !!!");
		marqueurs.figure = "YAMS !!!!!";
		joueur.figureIa = "YAMS";
	} else if (isBigSuite(dices) === true) {
		console.log("Big Suite");
		marqueurs.figure = "GRANDE SUITE !!!!";
		joueur.figureIa = "GRANDE SUITE";
	} else if (isLittleSuite(dices) === true) {
		console.log("Little Suite");
		marqueurs.figure = "PETITE SUITE !!!";
		joueur.figureIa = "PETITE SUITE";
	} else if (isFull(dices) === true) {
		console.log("Full");
		marqueurs.figure = "FULL !!";
		joueur.figureIa = "FULL";
	} else if (is4DicesEquals(dices) === true) {
		console.log("Quadrupe");
		marqueurs.figure = "CARRE !";
		joueur.figureIa = "CARRE";
	} else if (is3DicesEquals(dices) === true) {
		console.log("Triple");
		marqueurs.figure = "TRIPLE ";
		joueur.figureIa = "TRIPLE";
	} else if (is2DicesEquals(dices) === true) {
		console.log("Double");
		marqueurs.figure = "CHANCE";
		joueur.figureIa = "CHANCE";
	} else {
		marqueurs.figure = " ";
	}


	function isYams(dices) {
		var yams = true;
		for (var i = 0; i < (dices.length - 1) && yams; i++) {
			var dice = dices[i];
			var nextDice = dices[i + 1];

			if (dice != nextDice) {
				yams = false;
			}
		}
		return yams;
	}

	function isBigSuite(dices) {
		var bigSuite = true;
		var sortedDices = dices.slice().sort();
		var expectedNumber = 2;
		for (var i = 0; i < sortedDices.length && bigSuite; i++) {
			var dice = sortedDices[i];
			if (dice != expectedNumber) {
				bigSuite = false;
			}
			expectedNumber++;
		}
		return bigSuite;
	}

	function isLittleSuite(dices) {
		var littleSuite = true;
		var sortedDices = dices.slice().sort();
		var expectedNumber = 1;
		for (var i = 0; i < sortedDices.length && littleSuite; i++) {
			var dice = sortedDices[i];
			if (dice != expectedNumber) {
				littleSuite = false;
			}
			expectedNumber++;
		}
		return littleSuite;
	}

	function is4DicesEquals(dices) {
		return isEnoughtDicesEquals(dices, 4);
	}

	function is3DicesEquals(dices) {
		return isEnoughtDicesEquals(dices, 3);
	}

	function is2DicesEquals(dices) {
		return isEnoughtDicesEquals(dices, 2);
	}

	function isEnoughtDicesEquals(dices, hits) {
		hitsFound = false;
		for (value = 1;
				(value <= 6) && !hitsFound; value++) {
			hitCounter = 0;
			for (number = 0; number < dices.length; number++) {
				var dice = dices[number];
				if (value === dice) {
					hitCounter++;
				}
			}
			if (hitCounter === hits) {
				hitsFound = true;
				moredicesIa = value;
				joueur.moredicesIa = moredicesIa;
			}
		}
		return hitsFound;
	}

	function isFull(dices) {
		return is2DicesEquals(dices) && is3DicesEquals(dices);
	}

	function trydice() {
		var cdices = [0, 0, 0, 0, 0, 0];

		for (i = 0; i < dices.length; i++) {
			for (p = 0; p < cdices.length; p++) {
				if (dices[i] - 1 === p) {
					cdices[p] = (cdices[p] + 1)
				}
			}
		}
		var t = Math.max.apply(Math, cdices);

		hdices = t;

		for (i = 0; i < cdices.length; i++) {
			p = (i + 1);
			console.log(cdices[i] + " des de " + (i + 1));
			if (t <= cdices[i]) {
				hdices = (p);
			}
		}
	}


	function trypair() {

		firstpair = 0;
		secondpair = 0;
		trypair = dices.sort();

		for (i = 0; i < dices.length - 1; i++) {
			p = i + 1;
			if (dices[i] === dices[i + 1] && firstpair === 0) {
				firstpair = dices[i];
			}
		}

		for (i = 0; i < dices.length; i++) {
			p = (i + 1);
			if (dices[i] === dices[p] && dices[i] != firstpair) {
				secondpair = dices[i];
			}
		}
		if (secondpair === 0) {
			firstpair = 0;
		}
	}

	joueur.lanceria++;
	fs.writeFileSync(query.pseudo + ".json", JSON.stringify(joueur), "UTF-8");
	joueur = JSON.parse(fs.readFileSync(query.pseudo + ".json", "utf-8"));
	marqueurs.dices1 = "<img src='dice" + joueur.dicesIa[0] + ".png' style='width: 70px; height: 70px'>";
	marqueurs.dices2 = "<img src='dice" + joueur.dicesIa[1] + ".png' style='width: 70px; height: 70px'>";
	marqueurs.dices3 = "<img src='dice" + joueur.dicesIa[2] + ".png' style='width: 70px; height: 70px'>";
	marqueurs.dices4 = "<img src='dice" + joueur.dicesIa[3] + ".png' style='width: 70px; height: 70px'>";
	marqueurs.dices5 = "<img src='dice" + joueur.dicesIa[4] + ".png' style='width: 70px; height: 70px'>";

	marqueurs.figureJ = joueur.figureJ;
	marqueurs.dicesJ1 = "<img src='dice" + joueur.dices[0] + ".png' style='width: 70px; height: 70px'>";
	marqueurs.dicesJ2 = "<img src='dice" + joueur.dices[1] + ".png' style='width: 70px; height: 70px'>";
	marqueurs.dicesJ3 = "<img src='dice" + joueur.dices[2] + ".png' style='width: 70px; height: 70px'>";
	marqueurs.dicesJ4 = "<img src='dice" + joueur.dices[3] + ".png' style='width: 70px; height: 70px'>";
	marqueurs.dicesJ5 = "<img src='dice" + joueur.dices[4] + ".png' style='width: 70px; height: 70px'>";
	marqueurs.lanceria = joueur.lanceria;

	totalJ = joueur.dices[0] + joueur.dices[1] + joueur.dices[2] + joueur.dices[3] + joueur.dices[4];
	totalIa = joueur.dicesIa[0] + joueur.dicesIa[1] + joueur.dicesIa[2] + joueur.dicesIa[3] + joueur.dicesIa[4];

	if (joueur.lanceria >= 3) {
		page = fs.readFileSync('modele_jeu_solo_resultat_final.html', 'UTF-8');
	}

	var joueur = JSON.parse(fs.readFileSync(query.pseudo + ".json", "utf-8"));

	if (joueur.figureJ === "YAMS" && joueur.figureIa === "YAMS") {
		if (joueur.moredices > joueur.moredicesIa) {
			marqueurs.resultat = "Vous avez gagné !";
			if (joueur.lanceria >= 3) {}
		} else if (joueur.moredices === joueur.moredicesIa && totalJ === totalIa) {
			marqueurs.resultat = "Egalite";
		} else {
			marqueurs.resultat = "Vous avez perdu";
		}
	}

	if (joueur.figureJ === "FULL" && joueur.figureIa === "FULL") {
		if (joueur.moredices < joueur.moredicesIa) {
			marqueurs.resultat = "Vous avez perdu !";
		} else if (joueur.moredices === joueur.moredicesIa && totalJ === totalIa) {
			marqueurs.resultat = "Egalite";
		} else {
			marqueurs.resultat = "Vous avez gagne !";
		}
	}

	if (joueur.figureJ === "CARRE" && joueur.figureIa === "CARRE") {
		if (joueur.moredices > joueur.moredicesIa) {
			marqueurs.resultat = "Vous avez gagné !";
		} else if (joueur.moredices === joueur.moredicesIa && totalJ === totalIa) {
			marqueurs.resultat = "Egalite";
		} else {
			marqueurs.resultat = "Vous avez perdu";
		}
	}
	if (joueur.figureJ === "TRIPLE" && joueur.figureIa === "TRIPLE") {
		if (joueur.moredices > joueur.moredicesIa) {
			marqueurs.resultat = "Vous avez gagné !";
		} else if (joueur.moredices === joueur.moredicesIa && totalJ === totalIa) {
			marqueurs.resultat = "Egalite";
		} else {
			marqueurs.resultat = "Vous avez perdu";
		}
	}

	if (joueur.figureJ === "CHANCE" && joueur.figureIa === "CHANCE") {
		if (totalJ > totalIa) {
			if (joueur.lanceria >= 3) {
				marqueurs.resultat = "Vous avez gagné !";
			}
		} else if (totalJ === totalIa) {
			marqueurs.resultat = "Egalite";
		} else {
			marqueurs.resultat = "Vous avez perdu";
		}
	}

	if (joueur.figureJ === "YAMS") {
		if (joueur.figureIa === "FULL" || joueur.figureIa === "CARRE" || joueur.figureIa === "TRIPLE" || joueur.figureIa === "DOUBLE" || joueur.figureIa === "PETITE SUITE" || joueur.figureIa === "GRANDE SUITE" || joueur.figureIa === "CHANCE") {
			marqueurs.resultat = "Vous avez gagné !";
		}
	}
	if (joueur.figureJ === "GRANDE SUITE") {
		if (joueur.figureIa != "YAMS" && joueur.figureIa != "GRANDE SUITE" || joueur.figureIa === "CHANCE") {
			marqueurs.resultat = "Vous avez gagné !";
		}
	}
	if (joueur.figureJ === "PETITE SUITE") {
		if (joueur.figureIa === "FULL" || joueur.figureIa === "CARRE" || joueur.figureIa === "TRIPLE" || joueur.figureIa === "DOUBLE" || joueur.figureIa === "CHANCE") {
			marqueurs.resultat = "Vous avez gagné !";
		}
	}
	if (joueur.figureJ === "FULL") {
		if (joueur.figureIa === "CARRE" || joueur.figureIa === "TRIPLE" || joueur.figureIa === "DOUBLE" || joueur.figureIa === "CHANCE") {
			marqueurs.resultat = "Vous avez gagné !";
		}
	}
	if (joueur.figureJ === "CARRE") {
		if (joueur.figureIa === "TRIPLE" || joueur.figureIa === "DOUBLE" || joueur.figureIa === "CHANCE") {
			marqueurs.resultat = "Vous avez gagné !";
		}
	}
	if (joueur.figureJ === "TRIPLE") {
		if (joueur.figureIa === "DOUBLE" || joueur.figureIa === "CHANCE") {
			marqueurs.resultat = "Vous avez gagné !";
		}
	}

	if (joueur.lanceria === 3 && joueur.lancerJ === 3) {
		if (marqueurs.resultat != "Vous avez gagné !" && marqueurs.resultat != "Egalite") {
			joueur.dollar = Number(Number(joueur.dollar) - Number(joueur.bet));
			bank.dollar = Number(Number(bank.dollar) + Number(joueur.bet));
		} else {
			joueur.dollar = Number(Number(joueur.dollar) + Number(joueur.bet));
			bank.dollar = Number(Number(bank.dollar) - Number(joueur.bet));
		}
	}

	fs.writeFileSync(query.pseudo + ".json", JSON.stringify(joueur), "UTF-8");
	fs.writeFileSync("bank.json", JSON.stringify(bank), "UTF-8");

	marqueurs.dollar = joueur.dollar;
	marqueurs.pseudo = query.pseudo;
	marqueurs.password = query.password;
	page = page.supplant(marqueurs);

	res.writeHead(200, {
			'Content-Type': 'text/html'
			});
	res.write(page);
	res.end();
};

module.exports = trait;
