"use strict";

var fs = require('fs');
require('remedial');

var commencer = function (req, res, query) {

var page = fs.readFileSync('commencer.html','utf-8');

res.writeHead(200, {'Content-Type': 'text/html'});
res.write(page);
res.end();

};

module.exports = commencer;
