"use strict";

var http = require("http");
var url = require("url");
var querystring = require("querystring");

//-------------------------------------------------------------------------
//// DECLARATION DES DIFFERENTS MODULES CORRESPONDANT A CHAQUE ACTION
////-------------------------------------------------------------------------
//
var req_commencer = require("./req_commencer.js");
var req_static = require("./req_static.js");
var req_erreur = require("./req_erreur.js");
var req_jouer = require('./req_jouer');
////-------------------------------------------------------------------------
//// FONCTION DE CALLBACK APPELLEE POUR CHAQUE REQUETE
////-------------------------------------------------------------------------

var traite_requete = function (req, res) {

  	var ressource;
    var requete;
    var pathname;
  	var query;

console.log("URL reçue : " + req.url);
requete = url.parse(req.url, true);
pathname = requete.pathname;
query = requete.query;
console.log("pathname : " + pathname);
console.log("query string (pseudo) : " + query.pseudo);
console.log("query string (password) : " + query.password);

// ROUTEUR

try {
	switch (pathname) {
		case '/':
		case '/req_commencer':
			req_commencer(req, res, query);
			break;
		case '/req_jouer':
			req_jouer(req, res, query);
			break;
		default:
			req_static(req, res, pathname);
			break;
	}
} catch (e) {
	console.log('Erreur : ' + e.stack);
	console.log('Erreur : ' + e.message);
	console.trace();
	req_erreur(req, res, query);
}
};

//-------------------------------------------------------------------------
// CREATION ET LANCEMENT DU SERVEUR
//-------------------------------------------------------------------------

var mon_serveur = http.createServer(traite_requete);
var port = 5000;
console.log("Serveur en ecoute sur port " + port);
mon_serveur.listen(port)
