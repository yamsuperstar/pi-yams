// PERMET L'AFFICHAGE DES PAGES DU DEFI

"use strict";

var fs = require("fs");
require('remedial');

var trait = function (req, res, query) {

	var page;
	var marqueurs;
	var i;
	var connectes;
	var contenu;

	// ON CHANGE L'ÉTAT DES JOUEURS DANS LE JSON

	contenu = fs.readFileSync("connectes.json","UTF-8");
	connectes = JSON.parse(contenu);

	for(i=0; i<connectes.length; i++) {

		if(connectes[i].joueur === query.pseudo) {
			connectes[i].etat = "defi";
			connectes[i].adv = query.adv;

			page = fs.readFileSync('modele_attente_defi.html', 'utf-8');
			marqueurs = {};
			marqueurs.pseudo = query.pseudo;
			marqueurs.adv = connectes[i].adv;
			page = page.supplant(marqueurs);

		} else if (connectes[i].joueur === query.adv) {
			connectes[i].etat = "defie";
			connectes[i].adv = query.pseudo;
		} 
	}

	contenu = JSON.stringify(connectes);
	fs.writeFileSync("connectes.json", contenu, "utf-8");

	res.writeHead(200, {'Content-Type' : 'text/html'});
	res.write(page);
	res.end();
};
//--------------------------------------------------------------------------
module.exports = trait;
