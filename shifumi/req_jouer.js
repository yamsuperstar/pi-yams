"use strict";

var fs = require("fs");
require('remedial');

var jouer = function (req, res, query) {

var page;
var figure = [];
var marqueurs = {};
var choixJ;

page = fs.readFileSync("jeu.html","utf-8");


figure[0] = "Pierre";
figure[1] = "Ciseaux";
figure[2] = "Feuille";

var choix = Math.floor(Math.random() * 3);
console.log(choix + " ==== CHOIX");
console.log(figure + " ====== FIGURE");

var score = JSON.parse(fs.readFileSync("score.json", "utf-8"));

choix = figure[choix];
choixJ = query.Choix;
console.log(choixJ + "CHOIX FIGURE JOUEUR");
console.log(choix + "CHOIX FIGURE ORDI");

if(choixJ === "Pierre") { 
if(choix === "Feuille") {
marqueurs.resultat = "L'IA QUI GAGNE";
score.pointIA++;
}
if(choix === "Ciseaux") {
marqueurs.resultat = "VOUS QUI GAGNEZ";
score.pointJoueur++;
}
if(choix === "Pierre") {
marqueurs.resultat = "UNE EGALITE";
}
}

if(choixJ === "Feuille") { 
if(choix === "Feuille") {
marqueurs.resultat = "UNE EGALITE";
}
if(choix === "Ciseaux") {
marqueurs.resultat = "L'IA QUI GAGNE";
score.pointIA++;
}
if(choix === "Pierre") {
marqueurs.resultat = "VOUS QUI GAGNEZ";
score.pointJoueur++;
}
}

if(choixJ === "Ciseaux") { 
if(choix === "Feuille") {
marqueurs.resultat = "VOUS QUI GAGNEZ";
score.pointJoueur++;
}
if(choix === "Ciseaux") {
marqueurs.resultat = "UNE EGALITE";
}
if(choix === "Pierre") {
marqueurs.resultat = "L'IA QUI GAGNE";
score.pointIA++;
}
}

fs.writeFileSync("score.json", JSON.stringify(score), "UTF-8");

marqueurs.choixIA = choix;
marqueurs.scoreIA = score.pointIA;
marqueurs.scoreJ = score.pointJoueur;
page = page.supplant(marqueurs);

res.writeHead(200, {'Content-Type': 'text/html'});
res.write(page);
res.end();
};

module.exports = jouer;
