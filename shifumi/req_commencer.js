"use strict";

var fs = require("fs");
require('remedial');

var commencer = function (req, res, query) {

	var page;
	var score = JSON.parse(fs.readFileSync("score.json", "utf-8"));

	page = fs.readFileSync("accueil.html","utf-8");

	score.pointJoueur = Number(0);
	score.pointIA = Number(0);

	score = JSON.stringify(score);
	fs.writeFileSync("score.json", score , "UTF-8");

	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write(page);
	res.end();
};

module.exports = commencer;
