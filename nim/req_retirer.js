"use strict";

var fs = require("fs");
require('remedial');

var req_retirer = function (req, res, query) {

	var page;
	var lireJSON;
	var marqueurs;
	var i;
	var allumette;
	var affichage;
	var nbmaxi;
	var joueur;
	var Player;


	allumette = fs.readFileSync("allumettes.json", "UTF-8");
	allumette = Number(allumette);

	console.log(allumette + "====== ALLUMETTES");
	console.log(query.combien + "===== QUERY");
	affichage = "";

	Player = query.Player;
	if ( Player === "1") {
		Player = "2";
	} else {
		Player = "1";
	}

	if (allumette > 4) {
		page = fs.readFileSync("modele_accueil_nim.html", "UTF-8");
	} else if(allumette <= 4) {
		page = fs.readFileSync("modele_fin_nim.html", "UTF-8");
	}

	allumette = allumette - query.combien;

	//}
	for(i=0; i<allumette; i++) {
		affichage += '<img src= allumette.png>';
	}

	console.log(allumette + "===== NB MAXI");
	console.log(query.Player + "===== QUERY PLAYER");	


	fs.writeFileSync("allumettes.json", allumette , "UTF-8");
	marqueurs = {};
	marqueurs.allumette = affichage;
	marqueurs.nbmaxi = allumette;
	marqueurs.player = Player;

	page = page.supplant(marqueurs);

	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write(page);
	res.end();
};

module.exports = req_retirer;
