"use strict";

var fs = require('fs');
require('remedial');

var trait = function (req, res, query) {
    var page;
    var marqueurs = {};
    var joueur = JSON.parse(fs.readFileSync(query.pseudo + ".json", "utf-8"));
    joueur.pseudo = query.pseudo;

    marqueurs.dices1 = "<img src='dice" + joueur.dicesIa[0] + ".png' style='width: 70px; height: 70px'>";
    marqueurs.dices2 = "<img src='dice" + joueur.dicesIa[1] + ".png' style='width: 70px; height: 70px'>";
    marqueurs.dices3 = "<img src='dice" + joueur.dicesIa[2] + ".png' style='width: 70px; height: 70px'>";
    marqueurs.dices4 = "<img src='dice" + joueur.dicesIa[3] + ".png' style='width: 70px; height: 70px'>";
    marqueurs.dices5 = "<img src='dice" + joueur.dicesIa[4] + ".png' style='width: 70px; height: 70px'>";
    marqueurs.lanceria = joueur.lanceria;

    page = fs.readFileSync("modele_jeu_solo_passif.html", "utf-8");
    marqueurs.pseudo = query.pseudo;
    marqueurs.password = query.password;
    page = page.supplant(marqueurs);
    res.writeHead(200, {
        'Content-Type': 'text/html'
    });
    res.write(page);
    res.end();
};
module.exports = trait;