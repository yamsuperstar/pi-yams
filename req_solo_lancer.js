"use strict";

var fs = require("fs");
require('remedial');
var marqueurs;
var trait = function (req, res, query) {
    var contenu;
    var i;
    var dices = [];
    var contenu_fichier;
    var listeMembres;
    var marqueurs;
    var page;
    var isYams;
    var isBigSuite;
    var isLittleSuite;
    var sortedDices;
    var value;
    var number;
    var isFull;
    var hitCounter;
    var isEnoughtDicesEquals;
    var hitsFound;
    var figureJ;
    var moredices;
    marqueurs = {};






    var joueur = JSON.parse(fs.readFileSync(query.pseudo + ".json", "utf-8"));
    contenu_fichier = fs.readFileSync("membres.json", "utf-8");
    listeMembres = JSON.parse(contenu_fichier);
    page = fs.readFileSync('modele_jeu_solo_actif.html', 'UTF-8');

    if (query.de1 === query.po || joueur.lancerJ === 0) {
        joueur.dices[0] = Math.floor(Math.random() * 6) + 1;
    }
    if (query.de2 === query.po || joueur.lancerJ === 0) {
        joueur.dices[1] = Math.floor(Math.random() * 6) + 1;
    }
    if (query.de3 === query.po || joueur.lancerJ === 0) {
        joueur.dices[2] = Math.floor(Math.random() * 6) + 1;
    }
    if (query.de4 === query.po || joueur.lancerJ === 0) {
        joueur.dices[3] = Math.floor(Math.random() * 6) + 1;
    }
    if (query.de5 === query.po || joueur.lancerJ === 0) {
        joueur.dices[4] = Math.floor(Math.random() * 6) + 1;
    }


//ON ATTRIBUT DES VALEURES AUX MARQUEURS

    marqueurs.dices1 = "<img src='dice" + joueur.dices[0] + ".png' style='width: 70px; height: 70px'>";
    marqueurs.dices2 = "<img src='dice" + joueur.dices[1] + ".png' style='width: 70px; height: 70px'>";
    marqueurs.dices3 = "<img src='dice" + joueur.dices[2] + ".png' style='width: 70px; height: 70px'>";
    marqueurs.dices4 = "<img src='dice" + joueur.dices[3] + ".png' style='width: 70px; height: 70px'>";
    marqueurs.dices5 = "<img src='dice" + joueur.dices[4] + ".png' style='width: 70px; height: 70px'>";




    dices = joueur.dices;
    joueur.figureJ = "CHANCE";

//ON ATTRIBUT LES MARQUEURS SELON LES FIGURES

    if (isYams(dices) === true) {
        marqueurs.figureJ = "YAMS !!!!!";
        joueur.figureJ = "YAMS";
    } else if (isBigSuite(dices) === true) {
        marqueurs.figureJ = "GRANDE SUITE !!!!";
        joueur.figureJ = "GRANDE SUITE";
    } else if (isLittleSuite(dices) === true) {
        marqueurs.figureJ = "PETITE SUITE !!!";
        joueur.figureJ = "PETITE SUITE";
    } else if (isFull(dices) === true) {
        marqueurs.figureJ = "FULL !!";
        joueur.figureJ = "FULL";
    } else if (is4DicesEquals(dices) === true) {
        marqueurs.figureJ = "CARRE !";
        joueur.figureJ = "CARRE";
    } else if (is3DicesEquals(dices) === true) {
        marqueurs.figureJ = "TRIPLE !!!";
        joueur.figureJ = "TRIPLE";
    } else if (is2DicesEquals(dices) === true) {
        marqueurs.figureJ = "CHANCE";
        joueur.figureJ = "CHANCE";
    } else {
        marqueurs.figureJ = " ";
    }




// ON VERIFIE SI LE JET DE DES DU JOUEUR CONTIENT CHAQUE FIGURE

    function isYams(dices) {
        var yams = true;
        for (var i = 0; i < (dices.length - 1) && yams; i++) {
            var dice = dices[i];
            var nextDice = dices[i + 1];

            if (dice != nextDice) {
                yams = false;
            }
        }
        return yams;
    }

    function isBigSuite(dices) {
        var bigSuite = true;
        var sortedDices = dices.slice().sort();
        var expectedNumber = 2;
        for (var i = 0; i < sortedDices.length && bigSuite; i++) {
            var dice = sortedDices[i];
            if (dice != expectedNumber) {
                bigSuite = false;
            }
            expectedNumber++;
        }
        return bigSuite;
    }

    function isLittleSuite(dices) {
        var littleSuite = true;
        var sortedDices = dices.slice().sort();
        var expectedNumber = 1;
        for (var i = 0; i < sortedDices.length && littleSuite; i++) {
            var dice = sortedDices[i];
            if (dice != expectedNumber) {
                littleSuite = false;
            }
            expectedNumber++;
        }
        return littleSuite;
    }


    function is4DicesEquals(dices) {
        return isEnoughtDicesEquals(dices, 4);
    }

    function is3DicesEquals(dices) {
        return isEnoughtDicesEquals(dices, 3);
    }

    function is2DicesEquals(dices) {
        return isEnoughtDicesEquals(dices, 2);
    }

    function isEnoughtDicesEquals(dices, hits) {
        hitsFound = false;
        for (value = 1;
            (value <= 6) && !hitsFound; value++) {
            hitCounter = 0;
            for (number = 0; number < dices.length; number++) {
                var dice = dices[number];
                if (value === dice) {
                    hitCounter++;
                }
            }
            if (hitCounter === hits) {
                hitsFound = true;
                moredices = value;
                joueur.moredices = moredices;
                console.log(moredices + " = MOREDICES ");
            }
        }
        return hitsFound;
    }

    function isFull(dices) {
        return is2DicesEquals(dices) && is3DicesEquals(dices);
    }

    function myButton() {
        var die1 = document.getElementById("die1");
        var die2 = document.getElementById("die2");
        var die3 = document.getElementById("die3");
        var die4 = document.getElementById("die4");
        var die5 = document.getElementById("die5");
        var status = document.getElementById("status");
        var d1 = Math.floor(Math.random() * 6) + 1;
        var d2 = Math.floor(Math.random() * 6) + 1;
        var d3 = Math.floor(Math.random() * 6) + 1;
        var d4 = Math.floor(Math.random() * 6) + 1;
        var d5 = Math.floor(Math.random() * 6) + 1;
        var diceTotal = d1 + d2 + d3 + d4 + d5;
        die1.innerHTML = d1;
        die2.innerHTML = d2;
        die3.innerHTML = d3;
        die4.innerHTML = d4;
        die5.innerHTML = d5;
        status.innerHTML = "You rolled " + diceTotal + ".";
        if (d1 == d2 == d3 == d4 == d5) {
            status.innerHTML += " DOUBLES! You get a free turn!!";
        }
    }

    joueur.lancerJ++;
    marqueurs.lancerJ = joueur.lancerJ;
    fs.writeFileSync(query.pseudo + ".json", JSON.stringify(joueur), "UTF-8");



// SI LE JOUEUR EST A SON 3EME LANCER, ON AFFICHE LA PAGE DE RESULTAT
    if (joueur.lancerJ >= 3) {
        page = fs.readFileSync('modele_jeu_solo_resultat_joueur.html', 'UTF-8');
    }
    marqueurs.pseudo = query.pseudo;
    marqueurs.password = query.password;
    page = page.supplant(marqueurs);
    console.log(marqueurs);


    res.writeHead(200, {
        'Content-Type': 'text/html'
    });
    res.write(page);
    res.end();
}

module.exports = trait;
