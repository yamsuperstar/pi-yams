"use strict";

var fs = require('fs');
require('remedial');

var trait = function (req, res, query) {

	var page;
	var marqueurs;
	var contenu;
	var connectes;
	var i;

	contenu = fs.readFileSync("connectes.json","utf-8");
	connectes = JSON.parse(contenu);

	for (i=0; i < connectes.length; i++) {
		if(connectes[i].etat === "libre"){

			page = fs.readFileSync("modele_annuler_defi.html","utf-8");
			marqueurs = {};
			marqueurs.pseudo = query.pseudo;
			marqueurs.password = query.password;
			page = page.supplant(marqueurs);
		
		} else if(connectes[i].etat === "defie"){

			page = fs.readFileSync("modele_demande_defi.html","utf-8");
			marqueurs = {};
			marqueurs.pseudo = query.pseudo;
			marqueurs.password = query.password;
			marqueurs.adv = connectes[i].adv;
			page = page.supplant(marqueurs);

		}
	}

	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write(page);
	res.end();
};

module.exports = trait;
