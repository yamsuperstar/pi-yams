"use strict";

var fs = require('fs');
require('remedial');

var trait = function (req, res, query) {

	var connectes;
	var contenu;
	var i;
	var page;
	var marqueurs;

	contenu = fs.readFileSync("connectes.json","UTF-8");
	connectes = JSON.parse(contenu);
	var joueur = JSON.parse(fs.readFileSync(query.pseudo + "multi.json","utf-8"));

	for(i=0; i<connectes.length; i++) {
		if(connectes[i].etat === "defie") {
			page = fs.readFileSync("modele_jeu_multi_actif.html", "utf-8");
			connectes[i].etat = "multi1";
			marqueurs = {};
			marqueurs.pseudo = query.pseudo;
			marqueurs.password = query.password;
			marqueurs.dices1 = 1;
			marqueurs.dices2 = 1;
			marqueurs.dices3 = 1;
			marqueurs.dices4 = 1;
			marqueurs.dices5 = 1;
			marqueurs.figureJ = " ";
			marqueurs.adv = connectes[i].adv;
			page = page.supplant(marqueurs);
		} if(connectes[i].etat === "defi") {
			connectes[i].etat = "multi2";
		}
	}

	contenu = JSON.stringify(connectes);
	fs.writeFileSync("connectes.json", contenu, "utf-8");

	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write(page);
	res.end();
};

module.exports = trait;
