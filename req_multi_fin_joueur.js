"use strict";

var fs = require('fs');
require('remedial');

var trait = function (req, res, query) {
	var page;
	var contenu;
	var connectes;
	var i;
	var marqueurs = {};
	var joueur;
	var contenu_fichier;
	var totalJ;
	var totalIa;
	var bank;
	var dollar;

	contenu = fs.readFileSync("connectes.json","UTF-8");
	connectes = JSON.parse(contenu);

	marqueurs.resultat = "Vous avez perdu";

	var joueur = JSON.parse(fs.readFileSync(query.pseudo + "multi.json", "utf-8"));


	for(i=0; i<connectes.length; i++) {
		if(connectes[i].etat === "multi2") {
			connectes[i].etat = "multi1";
		} if(connectes[i].joueur === query.pseudo) {
			connectes[i].etat = "fin";
		}

	}

	contenu = JSON.stringify(connectes);
	fs.writeFileSync("connectes.json", contenu, "utf-8");

	if(joueur.lancerJ2 === 3) {
		var joueursolo = JSON.parse(fs.readFileSync(query.pseudo + ".json", "utf-8"));
		page = fs.readFileSync("modele_jeu_multi_resultat_final.html", "utf-8");


		marqueurs.dicesadv1 = "<img src='dice" + joueur.dicesJ2[0] + ".png' style='width: 70px; height: 70px'>";
		marqueurs.dicesadv2 = "<img src='dice" + joueur.dicesJ2[1] + ".png' style='width: 70px; height: 70px'>";
		marqueurs.dicesadv3 = "<img src='dice" + joueur.dicesJ2[2] + ".png' style='width: 70px; height: 70px'>";
		marqueurs.dicesadv4 = "<img src='dice" + joueur.dicesJ2[3] + ".png' style='width: 70px; height: 70px'>";
		marqueurs.dicesadv5 = "<img src='dice" + joueur.dicesJ2[4] + ".png' style='width: 70px; height: 70px'>";

		marqueurs.figureJ = joueur.figureJ1;
		marqueurs.dicesJ1 = "<img src='dice" + joueur.dicesJ1[0] + ".png' style='width: 70px; height: 70px'>";
		marqueurs.dicesJ2 = "<img src='dice" + joueur.dicesJ1[1] + ".png' style='width: 70px; height: 70px'>";
		marqueurs.dicesJ3 = "<img src='dice" + joueur.dicesJ1[2] + ".png' style='width: 70px; height: 70px'>";
		marqueurs.dicesJ4 = "<img src='dice" + joueur.dicesJ1[3] + ".png' style='width: 70px; height: 70px'>";
		marqueurs.dicesJ5 = "<img src='dice" + joueur.dicesJ1[4] + ".png' style='width: 70px; height: 70px'>";
		marqueurs.figureadv = joueur.figureJ2;

		totalJ = joueur.dicesJ1[0] + joueur.dicesJ1[1] + joueur.dicesJ1[2] + joueur.dicesJ1[3] + joueur.dicesJ1[4];
		totalIa = joueur.dicesJ2[0] + joueur.dicesJ2[1] + joueur.dicesJ2[2] + joueur.dicesJ2[3] + joueur.dicesJ2[4];


		if (joueur.figureJ1 === "YAMS" && joueur.figureJ2 === "YAMS") {
			if (joueur.moredicesJ1 > joueur.moredicesJ2) {
				marqueurs.resultat = "Vous avez gagné !";
				if (joueur.lancerJ1 >= 3) {}
			} else if (joueur.moredicesJ1 === joueur.moredicesJ2 && totalJ === totalIa) {
				marqueurs.resultat = "Egalite";
			} else {
				marqueurs.resultat = "Vous avez perdu";
			}
		}

		if (joueur.figureJ1 === "FULL" && joueur.figureJ2 === "FULL") {
			if (joueur.moredicesJ1 < joueur.moredicesJ2) {
				marqueurs.resultat = "Vous avez perdu !";
				if (joueur.lanceria >= 3) {}
			} else if (joueur.moredicesJ1 === joueur.moredicesJ2 && totalJ === totalIa) {
				marqueurs.resultat = "Egalite";
			} else {
				marqueurs.resultat = "Vous avez gagne !";
			}
		}

		if (joueur.figureJ1 === "CARRE" && joueur.figureJ2 === "CARRE") {
			if (joueur.moredicesJ1 > joueur.moredicesJ2) {
				marqueurs.resultat = "Vous avez gagné !";
				if (joueur.lanceria >= 3) {}
			} else if (joueur.moredicesJ1 === joueur.moredicesJ2 && totalJ === totalIa) {
				marqueurs.resultat = "Egalite";
			} else {
				marqueurs.resultat = "Vous avez perdu";
			}
		}
		if (joueur.figureJ1 === "TRIPLE" && joueur.figureJ2 === "TRIPLE") {
			if (joueur.moredicesJ1 > joueur.moredicesJ2) {
				marqueurs.resultat = "Vous avez gagné !";
				if (joueur.lanceria >= 3) {}
			} else if (joueur.moredicesJ1 === joueur.moredicesJ2 && totalJ === totalIa) {
				marqueurs.resultat = "Egalite";
			} else {
				marqueurs.resultat = "Vous avez perdu";
			}
		}

		if (joueur.figureJ1 === "CHANCE" && joueur.figureJ2 === "CHANCE") {
			if (totalJ > totalIa) {
				if (joueur.lancerJ2 >= 3) {
					marqueurs.resultat = "Vous avez gagné !";
				}
			} else if (totalJ === totalIa) {
				marqueurs.resultat = "Egalite";
			} else {
				marqueurs.resultat = "Vous avez perdu";
			}
		}

		if (joueur.figureJ1 === "YAMS") {
			if (joueur.figureJ2 === "FULL" || joueur.figureJ2 === "CARRE" || joueur.figureJ2 === "TRIPLE" || joueur.figureJ2 === "DOUBLE" || joueur.figureJ2 === "PETITE SUITE" || joueur.figureJ2 === "GRANDE SUITE" || joueur.figureJ2 === "CHANCE") {
				marqueurs.resultat = "Vous avez gagné !";
				if (joueur.lancerJ2 >= 3) {}
			}
		}
		if (joueur.figureJ1 === "GRANDE SUITE") {
			if (joueur.figureJ2 != "YAMS" && joueur.figureJ2 != "GRANDE SUITE" || joueur.figureJ2 === "CHANCE") {
				marqueurs.resultat = "Vous avez gagné !";
				if (joueur.lanceria >= 3) {}
			}
		}
		if (joueur.figureJ1 === "PETITE SUITE") {
			if (joueur.figureJ2 === "FULL" || joueur.figureJ2 === "CARRE" || joueur.figureJ2 === "TRIPLE" || joueur.figureJ2 === "DOUBLE" || joueur.figureJ2 === "CHANCE") {
				marqueurs.resultat = "Vous avez gagné !";
				if (joueur.lanceria >= 3) {}
			}
		}
		if (joueur.figureJ1 === "FULL") {
			if (joueur.figureJ2 === "CARRE" || joueur.figureJ2 === "TRIPLE" || joueur.figureJ2 === "DOUBLE" || joueur.figureJ2 === "CHANCE") {
				marqueurs.resultat = "Vous avez gagné !";
				if (joueur.lanceria >= 3) {}
			}
		}
		if (joueur.figureJ1 === "CARRE") {
			if (joueur.figureJ2 === "TRIPLE" || joueur.figureJ2 === "DOUBLE" || joueur.figureJ2 === "CHANCE") {
				marqueurs.resultat = "Vous avez gagné !";
				if (joueur.lanceria >= 3) {}
			}
		}
		if (joueur.figureJ1 === "TRIPLE") {
			if (joueur.figureJ2 === "DOUBLE" || joueur.figureJ2 === "CHANCE") {
				marqueurs.resultat = "Vous avez gagné !";
				if (joueur.lanceria >= 3) {}
			}
		}

		if (joueur.lancerJ2 >= 3) {
			if (marqueurs.resultat != "Vous avez gagné !" && marqueurs.resultat != "Egalite") {
				joueursolo.dollar = Number(Number(joueursolo.dollar) - Number(50));
			} else {
				joueursolo.dollar = Number(Number(joueursolo.dollar) + Number(50));
			}
		}

		joueur.dollar = joueursolo.dollar;

		fs.writeFileSync(query.pseudo + "multi.json", JSON.stringify(joueur), "UTF-8");
		//fs.writeFileSync("bank.json", JSON.stringify(bank), "UTF-8");

		marqueurs.pseudo = query.pseudo;
		marqueurs.dollar = joueur.dollar;
		marqueurs.password = query.password;
		marqueurs.adv = query.adv;
		page = page.supplant(marqueurs);

	}
	if(joueur.lancerJ1 === 3 && joueur.lancerJ2 !== 3){
		page = fs.readFileSync("modele_jeu_multi_passif.html", "utf-8");
		marqueurs.pseudo = query.pseudo;
		marqueurs.password = query.password;
		marqueurs.adv = query.adv;
		page = page.supplant(marqueurs);
	}

	//	marqueurs.dices1 = joueur.dicesIa[0];
	//	marqueurs.dices2 = joueur.dicesIa[1];
	//	marqueurs.dices3 = joueur.dicesIa[2];
	//	marqueurs.dices4 = joueur.dicesIa[3];
	//	marqueurs.dices5 = joueur.dicesIa[4];

	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write(page);
	res.end();
};
module.exports = trait;
