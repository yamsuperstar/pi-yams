"use strict"

var isYams;
var isBigSuite;
var isLittleSuite;
var dices = [];
var sortedDices;
var value;
var number;
var isFull;
var hitCounter;
var isEnoughtDicesEquals;
var hitsFound;
dices[0] = 1;
dices[1] = 1;
dices[2] = 1;
dices[3] = 4;
dices[4] = 5;

if(isYams(dices) === true) {
	console.log("YAMS !!!! +++++++");
}
else if(isBigSuite(dices) === true) {
	console.log("BIG SUITE ++++++!!");
} 
else if(isLittleSuite(dices) === true) {
	console.log("LITTLE SUITE +++++ !");
} 
else if(isFull(dices) === true) {
	console.log("Full ++++");
}
else if(is4DicesEquals(dices) === true) {
	console.log("QUADRUPLE +++");
} 
else if(is3DicesEquals(dices) === true) {
	console.log("TRIPLE ++");
} 
else if(is2DicesEquals(dices) === true) {
	console.log("DUAL +");
} else {
  console.log("????????");
}

function isYams(dices) {
	var yams = true;
	for (var i = 0; i < (dices.length - 1) && yams; i++) {
		var dice = dices[i];
		var nextDice = dices[i+1];
		
		if(dice != nextDice) {
			yams = false;
		}
	}
	return yams;
}

function isBigSuite(dices) {
	var bigSuite = true;
	
	// On copie puis trie le tableau pour simplifier l'algo
	var sortedDices = dices.slice().sort();
	var expectedNumber = 2;
	for (var i = 0; i < sortedDices.length && bigSuite; i++) {
		var dice = sortedDices[i];
		if(dice != expectedNumber) {
			bigSuite = false;
		}
		expectedNumber++;
	}
	
	return bigSuite;
}

function isLittleSuite(dices) {
	var littleSuite = true;	
	// On copie puis trie le tableau pour simplifier l'algo
	var sortedDices = dices.slice().sort();
	var expectedNumber = 1;
	for (var i = 0; i < sortedDices.length && littleSuite; i++) {
		var dice = sortedDices[i];
		if(dice != expectedNumber) {
			littleSuite = false;
		}
		expectedNumber++;
	}
	
	return littleSuite;
}

function is4DicesEquals(dices) {
  return isEnoughtDicesEquals(dices, 4); // 4 === a hits
}

function is3DicesEquals(dices) {
  return isEnoughtDicesEquals(dices, 3); // 3 === a hits
}

function is2DicesEquals(dices) {
  return isEnoughtDicesEquals(dices, 2); // 2 === a hits
}

function isEnoughtDicesEquals(dices, hits) {
  hitsFound = false;
  
  // On parcourt toutes les valeurs possibles du dé
	for (value = 1; (value <= 6) && !hitsFound; value++) {
	    hitCounter = 0; // Nb de hits pour la valeur actuel sur tous nos dés
	    
	    // On parcourt tous nos dés
  	  for(number = 0; number < dices.length; number++) {
	        var dice = dices[number];
	        
	        if(value === dice) {
	            hitCounter++;
	        }
	    }
	    
	    // Si on a trouvé notre valeur au moins 4 fois en regardant les dés
	    if(hitCounter === hits) {
	    console.log("On a trouvé hits(" + hits + ") dés de valeur(" + value + ")");
	        hitsFound = true;
	    }
	}
	
	return hitsFound;
}



function isFull(dices) {
	return is2DicesEquals(dices) && is3DicesEquals(dices);
}
