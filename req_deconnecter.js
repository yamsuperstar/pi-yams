"use strict";

var fs = require("fs");
require('remedial');

var trait = function (req, res, query) {
	var marqueurs;
	var pseudo;
	var password;
	var page;
	var contenu_fichier;
	var listeConnectes;
	var i;


	// ON MET LE JOUEUR EN "OCCUPE"
	
	contenu_fichier = fs.readFileSync("connectes.json", 'utf-8');
	listeConnectes = JSON.parse(contenu_fichier);

	for(i=0; i<listeConnectes.length; i++) {
		if(listeConnectes[i].joueur === query.pseudo) {
			listeConnectes[i].etat = "occupe";
		}
	}
	
	contenu_fichier = JSON.stringify(listeConnectes);
	fs.writeFileSync("connectes.json", contenu_fichier , "UTF-8");

	// AFFICHAGE DE LA PAGE DECONNECTER

	page = fs.readFileSync('modele_deconnection.html', 'utf-8');
	marqueurs = {};
	marqueurs.pseudo = query.pseudo;
	marqueurs.password = query.password;
	page = page.supplant(marqueurs);
	res.writeHead(200, {'Content-Type' : 'text/html'});
	res.write(page);
	res.end();

};
//-------------------------------------------------------------------
module.exports = trait;
