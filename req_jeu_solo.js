"use strict";

var fs = require("fs");
require('remedial');


var trait = function (req, res, query) {

    var necroz;
    var contenu;
    var i;
    var dices;
    var contenu_fichier;
    var listeMembres;
    var marqueurs;
    var page;
    var joueur;
    var joueur = {};
    var listeConnectes;
    var connectes;


    var joueur = JSON.parse(fs.readFileSync(query.pseudo + ".json", "utf-8"));

    // ON REMPLACE "LIBRE" PAR "OCCUPE" SI LE JOUEUR VA EN SOLO

    contenu_fichier = fs.readFileSync("connectes.json", 'utf-8');
    listeConnectes = JSON.parse(contenu_fichier);
    for (i = 0; i < listeConnectes.length; i++) {
        if (listeConnectes[i].joueur === query.pseudo) {
            listeConnectes[i].etat = "occupe";
        }
    }

    contenu_fichier = JSON.stringify(listeConnectes);
    fs.writeFileSync("connectes.json", contenu_fichier, "UTF-8");

    // ON ECRIT LES INFOS UTILES DANS UN LE FICHIER DU JOUEUR
    joueur.pseudo = query.pseudo;
    joueur.dices = [1, 1, 1, 1, 1];
    joueur.lancerJ = 0;
    joueur.lanceria = 0;
    joueur.dicesIa = [1, 1, 1, 1, 1];
    joueur.figureJ = "";
    joueur.figureIa = "";
    joueur.moredices = 2;
    joueur.hdices = 1;
    joueur.secondpair = 0;
    joueur.firstpair = 0;
    joueur.bet = query.bet;



    fs.writeFileSync(query.pseudo + ".json", JSON.stringify(joueur), "UTF-8");
    contenu_fichier = fs.readFileSync("membres.json", "utf-8");

    listeMembres = JSON.parse(contenu_fichier);
    page = fs.readFileSync('modele_jeu_solo_actif.html', 'UTF-8');

    //ON DONNE DES VALEURS AUX MARQUEURS
    marqueurs = {};
    marqueurs.lancerJ = joueur.lancerJ;
    marqueurs.figureJ = " ";
    marqueurs.dices1 = "<img src='dice" + joueur.dices[0] + ".png' style='width: 70px; height: 70px'>";
    marqueurs.dices2 = "<img src='dice" + joueur.dices[1] + ".png' style='width: 70px; height: 70px'>";
    marqueurs.dices3 = "<img src='dice" + joueur.dices[2] + ".png' style='width: 70px; height: 70px'>";
    marqueurs.dices4 = "<img src='dice" + joueur.dices[3] + ".png' style='width: 70px; height: 70px'>";
    marqueurs.dices5 = "<img src='dice" + joueur.dices[4] + ".png' style='width: 70px; height: 70px'>";

    marqueurs.pseudo = query.pseudo;
    marqueurs.password = query.password;
    page = page.supplant(marqueurs);

    res.writeHead(200, {
        'Content-Type': 'text/html'
    });
    res.write(page);
    res.end();
};

module.exports = trait;