"use strict";
var fs = require('fs');
require('remedial');

var trait = function (req, res, query) {
	var connectes;
	var contenu;
	var i;
	var page;
	var marqueurs;

	contenu = fs.readFileSync("connectes.json","UTF-8");
	connectes = JSON.parse(contenu);

	for(i=0; i<connectes.length; i++) {
		page = fs.readFileSync("modele_jeu_multi_passif.html", "utf-8");
		marqueurs = {};
		marqueurs.pseudo = query.pseudo;
		marqueurs.password = query.password;
		marqueurs.adv = connectes[i].adv;
	if(connectes[i].etat === "defie") {
		page = fs.readFileSync("modele_jeu_multi_actif.html", "utf-8");
		marqueurs = {};
		marqueurs.pseudo = query.pseudo;
		marqueurs.password = query.password;
		marqueurs.adv = connectes[i].adv;
	}
}
contenu = JSON.stringify(connectes);
fs.writeFileSync("connectes.json", contenu, "utf-8");

	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write(page);
	res.end();
};

module.exports = trait;

