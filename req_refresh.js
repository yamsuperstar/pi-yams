"use strict";

var fs = require("fs");
require('remedial');

var trait = function (req, res, query) {

	var connectes;
	var contenu;
	var etat;
	var liste;
	var page;
	var i;
	var etat;
	var marqueurs;
	var bank;

	contenu = fs.readFileSync("connectes.json","UTF-8");
	connectes = JSON.parse(contenu);
	bank = fs.readFileSync("bank.json","UTF-8");
	bank = JSON.parse(bank);
			marqueurs = {};
			marqueurs.bank = bank.dollar;

	liste = "";
	for(i=0; connectes.length > i; i++) {
		if(connectes[i].etat === "libre" && connectes[i].joueur != query.pseudo) {
			liste += "<a href=./req_defi?pseudo="+query.pseudo+"&password="+query.password+"&adv="+ connectes[i].joueur+">"+connectes[i].joueur+"</a>";
			liste += "<br>"
		} if (connectes[i].etat === "libre") { 
			page = fs.readFileSync("modele_accueil_membre.html","utf-8");
			var joueur = JSON.parse(fs.readFileSync(query.pseudo+".json","utf-8"));
			marqueurs.pseudo = query.pseudo;
			marqueurs.password = query.password;
			marqueurs.dollar = joueur.dollar;
			marqueurs.liste = liste;
			page = page.supplant(marqueurs);
		} else if (connectes[i].etat === "defie") {
			page = fs.readFileSync("modele_demande_defi.html","UTF-8");
			var joueur = JSON.parse(fs.readFileSync(query.pseudo+".json","utf-8"));
			marqueurs = {};
			marqueurs.pseudo = query.pseudo;
			marqueurs.password = query.password;
			marqueurs.dollar = joueur.dollar;
			marqueurs.adv = connectes[i].adv;
			page = page.supplant(marqueurs);
		}
	}

	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write(page);
	res.end();
};

module.exports = trait;
