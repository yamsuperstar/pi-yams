"use strict"

var fs = require("fs");
require('remedial');

var trait = function (req, res, query) {

	var marqueurs = {};
	var pseudo;
	var password;
	var page;
	var contenu_fichier;
	var joueur;
	var listeConnectes;
	var i;
	var connectes;
	var contenu;
	var contenu2;
	var joueur2;
	var j;
	var bank;


	page = fs.readFileSync('modele_accueil_membre.html', 'UTF-8');

	// ON REMPLACE "OCCUPE" PAR "LIBRE" QUAND LE JOUEUR RETOURNE SUR ACCUEIL_MEMBRE
	contenu_fichier = fs.readFileSync("connectes.json", 'utf-8');
	listeConnectes = JSON.parse(contenu_fichier);
	for(i=0; i<listeConnectes.length; i++) {
		if(listeConnectes[i].joueur === query.pseudo ) {
			listeConnectes[i].etat = "libre";
		}
	}
	
	contenu = fs.readFileSync(query.pseudo + ".json", "utf-8");
	joueur = JSON.parse(contenu);
	console.log(contenu);
		if(joueur.pseudo === query.pseudo) {
		joueur.lancerJ = 0;
		}
	
		contenu = JSON.stringify(joueur);
	fs.writeFileSync(query.pseudo + ".json", contenu , "UTF-8");

	contenu_fichier = JSON.stringify(listeConnectes);
	fs.writeFileSync("connectes.json", contenu_fichier , "UTF-8");
	
	contenu2 = fs.readFileSync(query.pseudo + "multi.json", "utf-8");
	joueur2 = JSON.parse(contenu2);
		if(joueur2.pseudo === query.pseudo){
			joueur2.lancerJ1 = 0;
			joueur2.lancerJ2 = 0;
		}
	contenu2 = JSON.stringify(joueur2);
	fs.writeFileSync(query.pseudo + "multi.json", contenu2,"utf-8");

	var joueur = JSON.parse(fs.readFileSync(query.pseudo + ".json", "utf-8"));
	bank = fs.readFileSync('bank.json', 'UTF-8');
	bank = JSON.parse(bank);
	marqueurs.liste = " ";
	marqueurs.dollar = joueur.dollar;
	marqueurs.pseudo = query.pseudo;
	marqueurs.bank = bank.dollar;
	marqueurs.password = query.password;


	page = page.supplant(marqueurs);

	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write(page);
	res.end();
};

module.exports = trait;
