"use strict";

var fs = require('fs');
require('remedial');

var trait = function (req, res, query){

var page;
var marqueurs;
var contenu;
var i;
var connectes;

	contenu = fs.readFileSync("connectes.json","utf-8");
	connectes = JSON.parse(contenu);

	page = fs.readFileSync("modele_annuler_defi.html","utf-8");
	marqueurs = {};
	marqueurs.pseudo = query.pseudo;
	marqueurs.password = query.password;
	marqueurs.adv = query.adv;
	page = page.supplant(marqueurs);

	for(i=0; i<connectes.length; i++){
		if(connectes[i].etat === "defie"){
			connectes[i].etat = "libre";
		}
	}

	contenu = JSON.stringify(connectes);
	fs.writeFileSync("connectes.json", contenu, "utf-8");

	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write(page);
	res.end();
};

module.exports = trait;
