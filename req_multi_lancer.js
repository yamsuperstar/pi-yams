"use strict";

var fs = require("fs");
require('remedial');
var marqueurs;
var trait = function (req, res, query) {
    var contenu;
    var i;
    var dices = [];
    var contenu_fichier;
    var listeMembres;
    var marqueurs;
    var page;
    var isYams;
    var isBigSuite;
    var isLittleSuite;
    var sortedDices;
    var value;
    var number;
    var isFull;
    var hitCounter;
    var isEnoughtDicesEquals;
    var hitsFound;
    var figureJ;
    var moredices;
    marqueurs = {};

	





    var joueur = JSON.parse(fs.readFileSync(query.pseudo + "multi.json", "utf-8"));
    contenu_fichier = fs.readFileSync("membres.json", "utf-8");
    listeMembres = JSON.parse(contenu_fichier);
    page = fs.readFileSync('modele_jeu_multi_actif.html', 'UTF-8');


    if (query.de1 === query.po || joueur.lancerJ1 === 0) {
        joueur.dicesJ1[0] = Math.floor(Math.random() * 6) + 1;
    }
    if (query.de2 === query.po || joueur.lancerJ1 === 0) {
        joueur.dicesJ1[1] = Math.floor(Math.random() * 6) + 1;
    }
    if (query.de3 === query.po || joueur.lancerJ1 === 0) {
        joueur.dicesJ1[2] = Math.floor(Math.random() * 6) + 1;
    }
    if (query.de4 === query.po || joueur.lancerJ1 === 0) {
        joueur.dicesJ1[3] = Math.floor(Math.random() * 6) + 1;
    }
    if (query.de5 === query.po || joueur.lancerJ1 === 0) {
        joueur.dicesJ1[4] = Math.floor(Math.random() * 6) + 1;
    }

    dices = joueur.dicesJ1;
    joueur.figureJ1 = "0";



    if (isYams(dices) === true) {
        console.log("YAMS !!!");
        marqueurs.figureJ = "YAMS !!!!!";
        joueur.figureJ1 = "YAMS";
    } else if (isBigSuite(dices) === true) {
        console.log("Big Suite");
        marqueurs.figureJ = "GRANDE SUITE !!!!";
        joueur.figureJ1 = "GRANDE SUITE";
    } else if (isLittleSuite(dices) === true) {
        console.log("Little Suite");
        marqueurs.figureJ = "PETITE SUITE !!!";
        joueur.figureJ1 = "PETITE SUITE";
    } else if (isFull(dices) === true) {
        console.log("Full");
        marqueurs.figureJ = "FULL !!";
        joueur.figureJ1 = "FULL";
    } else if (is4DicesEquals(dices) === true) {
        console.log("Quadrupe");
        marqueurs.figureJ = "CARRE !";
        joueur.figureJ1 = "CARRE";
    } else if (is3DicesEquals(dices) === true) {
        console.log("Triple");
        marqueurs.figureJ = "TRIPLE !!!";
        joueur.figureJ1 = "TRIPLE";
    } else if (is2DicesEquals(dices) === true) {
        console.log("Double");
        marqueurs.figureJ = "DOUBLE";
        joueur.figureJ1 = "DOUBLE";
    } else {
        marqueurs.figureJ = " ";
    }


    function isYams(dices) {
        var yams = true;
        for (var i = 0; i < dices.length-1 && yams; i++) {
            var dice = dices[i];
            var nextDice = dices[i + 1];

            if (dice != nextDice) {
                yams = false;
            }
        }
        return yams;
    }

    function isBigSuite(dices) {
        var bigSuite = true;
        var sortedDices = dices.slice().sort();
        var expectedNumber = 2;
        for (i = 0; i < sortedDices.length && bigSuite; i++) {
            var dice = sortedDices[i];
            if (dice != expectedNumber) {
                bigSuite = false;
            }
            expectedNumber++;
        }
        return bigSuite;
    }

    function isLittleSuite(dices) {
        var littleSuite = true;
        var sortedDices = dices.slice().sort();
        var expectedNumber = 1;
        for (var i = 0; i < sortedDices.length && littleSuite; i++) {
            var dice = sortedDices[i];
            if (dice != expectedNumber) {
                littleSuite = false;
            }
            expectedNumber++;
        }
        return littleSuite;
    }


    function is4DicesEquals(dices) {
        return isEnoughtDicesEquals(dices, 4);
    }

    function is3DicesEquals(dices) {
        return isEnoughtDicesEquals(dices, 3);
    }

    function is2DicesEquals(dices) {
        return isEnoughtDicesEquals(dices, 2);
    }

    function isEnoughtDicesEquals(dices, hits) {
        hitsFound = false;
        for (value = 1;
            (value <= 6) && !hitsFound; value++) {
            hitCounter = 0;
            for (number = 0; number < dices.length; number++) {
                var dice = dices[number];
                if (value === dice) {
                    hitCounter++;
                }
            }
            if (hitCounter === hits) {
                hitsFound = true;
                moredices = value;
                joueur.moredices = moredices;
                console.log(moredices + " = MOREDICES ");
            }
        }
        return hitsFound;
    }

    function isFull(dices) {
        return is2DicesEquals(dices) && is3DicesEquals(dices);
    }



    function myButton() {
        var die1 = document.getElementById("die1");
        var die2 = document.getElementById("die2");
        var die3 = document.getElementById("die3");
        var die4 = document.getElementById("die4");
        var die5 = document.getElementById("die5");
        var status = document.getElementById("status");
        var d1 = Math.floor(Math.random() * 6) + 1;
        var d2 = Math.floor(Math.random() * 6) + 1;
        var d3 = Math.floor(Math.random() * 6) + 1;
        var d4 = Math.floor(Math.random() * 6) + 1;
        var d5 = Math.floor(Math.random() * 6) + 1;
        var diceTotal = d1 + d2 + d3 + d4 + d5;
        die1.innerHTML = d1;
        die2.innerHTML = d2;
        die3.innerHTML = d3;
        die4.innerHTML = d4;
        die5.innerHTML = d5;
        status.innerHTML = "You rolled " + diceTotal + ".";
        if (d1 == d2 == d3 == d4 == d5) {
            status.innerHTML += " DOUBLES! You get a free turn!!";
        }
    }




    joueur.lancerJ1++;
    fs.writeFileSync(query.pseudo + "multi.json", JSON.stringify(joueur), "UTF-8");
    joueur = JSON.parse(fs.readFileSync(query.pseudo + "multi.json", "utf-8"));
    console.log(fs.readFileSync(query.pseudo + ".json", "utf-8"));
   marqueurs.dices1 = joueur.dicesJ1[0];
   marqueurs.dices2 = joueur.dicesJ1[1];
   marqueurs.dices3 = joueur.dicesJ1[2];
   marqueurs.dices4 = joueur.dicesJ1[3];
   marqueurs.dices5 = joueur.dicesJ1[4];


	console.log(joueur);
    if (joueur.lancerJ1 >= 3) {
        page = fs.readFileSync('modele_jeu_multi_resultat_joueur.html', 'UTF-8');
    }
    marqueurs.pseudo = query.pseudo;
    marqueurs.password = query.password;
	marqueurs.adv = query.adv;
	page = page.supplant(marqueurs);

    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(page);
    res.end();
};

module.exports = trait;
