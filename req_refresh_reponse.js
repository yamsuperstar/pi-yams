"use strict";

var fs = require('fs');
require('remedial');

var trait = function (req, res, query) {

	var page;
	var marqueurs = {};
	var contenu;
	var connectes;
	var i;
	var dicesJ2;
	var lancerJ2;
	var figureJ2;
	var moredicesJ2;
	var contenu_fichier;
	var lire;
	var contenu2;
	var	lireJ1;
	var totalJ;
	var totalIa;
	var bank;
	var dollar;


	marqueurs.resultat = "Vous avez perdu";

	contenu_fichier = fs.readFileSync(query.adv + "multi.json","utf-8");
	lire = JSON.parse(contenu_fichier);
	dicesJ2 = lire.dicesJ1;
	lancerJ2 = lire.lancerJ1;
	figureJ2 = lire.figureJ1;
	moredicesJ2 = lire.moredicesJ1;

	contenu2 = fs.readFileSync(query.pseudo + "multi.json","utf-8");
	lireJ1 = JSON.parse(contenu2);
	lireJ1.lancerJ2 = lancerJ2;
	lireJ1.figureJ2 = figureJ2;
	lireJ1.dicesJ2 = dicesJ2;
	lireJ1.moredicesJ2 = moredicesJ2;

	contenu2 = JSON.stringify(lireJ1);
	fs.writeFileSync(query.pseudo + "multi.json", contenu2,"utf-8");

	contenu = fs.readFileSync("connectes.json","utf-8");
	connectes = JSON.parse(contenu);

	for (i=0; i < connectes.length; i++) {
		if(connectes[i].joueur === query.pseudo && connectes[i].etat === "defi") {

			page = fs.readFileSync("modele_attente_defi.html","utf-8");
			marqueurs = {};
			marqueurs.pseudo = query.pseudo;
			marqueurs.password = query.password;
			marqueurs.adv = connectes[i].adv;
			page = page.supplant(marqueurs);

		} else if(connectes[i].joueur === query.adv && connectes[i].etat === "libre") {

			page = fs.readFileSync("modele_refus_defi.html","utf-8");
			marqueurs = {};
			marqueurs.pseudo = query.pseudo;
			marqueurs.password = query.password;
			marqueurs.adv = connectes[i].adv;
			page = page.supplant(marqueurs);

		} else if(connectes[i].joueur === query.pseudo && connectes[i].etat === "multi2") {

			page = fs.readFileSync("modele_jeu_multi_passif.html","utf-8");
			marqueurs = {};
			marqueurs.pseudo = query.pseudo;
			marqueurs.password = query.password;
			marqueurs.adv = connectes[i].adv;
			page = page.supplant(marqueurs);
		} else if(connectes[i].joueur === query.pseudo && connectes[i].etat === "multi1") {

			page = fs.readFileSync("modele_jeu_multi_actif.html","utf-8");
			marqueurs = {};
			marqueurs.pseudo = query.pseudo;
			marqueurs.password = query.password;
			marqueurs.adv = connectes[i].adv;
			marqueurs.dices1 = 1;
			marqueurs.dices2 = 1;
			marqueurs.dices3 = 1;
			marqueurs.dices4 = 1;
			marqueurs.dices5 = 1;
			marqueurs.figureJ = " ";
			page = page.supplant(marqueurs);
		} else if(lireJ1.lancerJ1 === 3 && lireJ1.lancerJ2 !== 3) {

			page = fs.readFileSync("modele_jeu_multi_passif.html","utf-8");
			marqueurs = {};
			marqueurs.pseudo = query.pseudo;
			marqueurs.password = query.password;
			marqueurs.adv = query.adv;
			page = page.supplant(marqueurs);
		} else if(lireJ1.lancerJ1 === 3 && lireJ1.lancerJ2 === 3) {
			var joueur = JSON.parse(fs.readFileSync(query.pseudo + "multi.json", "utf-8"));
			var joueursolo = JSON.parse(fs.readFileSync(query.pseudo + ".json", "utf-8"));
			page = fs.readFileSync("modele_jeu_multi_resultat_final.html","utf-8");

			marqueurs.dicesadv1 = "<img src='dice" + joueur.dicesJ2[0] + ".png' style='width: 70px; height: 70px'>";
			marqueurs.dicesadv2 = "<img src='dice" + joueur.dicesJ2[1] + ".png' style='width: 70px; height: 70px'>";
			marqueurs.dicesadv3 = "<img src='dice" + joueur.dicesJ2[2] + ".png' style='width: 70px; height: 70px'>";
			marqueurs.dicesadv4 = "<img src='dice" + joueur.dicesJ2[3] + ".png' style='width: 70px; height: 70px'>";
			marqueurs.dicesadv5 = "<img src='dice" + joueur.dicesJ2[4] + ".png' style='width: 70px; height: 70px'>";

			marqueurs.figureJ = joueur.figureJ1;
			marqueurs.dicesJ1 = "<img src='dice" + joueur.dicesJ1[0] + ".png' style='width: 70px; height: 70px'>";
			marqueurs.dicesJ2 = "<img src='dice" + joueur.dicesJ1[1] + ".png' style='width: 70px; height: 70px'>";
			marqueurs.dicesJ3 = "<img src='dice" + joueur.dicesJ1[2] + ".png' style='width: 70px; height: 70px'>";
			marqueurs.dicesJ4 = "<img src='dice" + joueur.dicesJ1[3] + ".png' style='width: 70px; height: 70px'>";
			marqueurs.dicesJ5 = "<img src='dice" + joueur.dicesJ1[4] + ".png' style='width: 70px; height: 70px'>";
			marqueurs.figureadv = joueur.figureJ2;

			totalJ = joueur.dicesJ1[0] + joueur.dicesJ1[1] + joueur.dicesJ1[2] + joueur.dicesJ1[3] + joueur.dicesJ1[4];
			totalIa = joueur.dicesJ2[0] + joueur.dicesJ2[1] + joueur.dicesJ2[2] + joueur.dicesJ2[3] + joueur.dicesJ2[4];

			console.log(totalIa + "===== TOTAL IA");
			console.log(totalJ + "===== TOTAL J");
			if (joueur.figureJ1 === "YAMS" && joueur.figureJ2 === "YAMS") {
				if (joueur.moredicesJ1 > joueur.moredicesJ2) {
					marqueurs.resultat = "Vous avez gagné !";
					if (joueur.lancerJ1 >= 3) {}
				} else if (joueur.moredicesJ1 === joueur.moredicesJ2 && totalJ === totalIa) {
					marqueurs.resultat = "Egalite";
				} else {
					marqueurs.resultat = "Vous avez perdu";
				}
			}

			if (joueur.figureJ1 === "FULL" && joueur.figureJ2 === "FULL") {
				if (joueur.moredicesJ1 < joueur.moredicesJ2) {
					marqueurs.resultat = "Vous avez perdu !";
					if (joueur.lancerJ2 >= 3) {}
				} else if (joueur.moredicesJ1 === joueur.moredicesJ2 && totalJ === totalIa) {
					marqueurs.resultat = "Egalite";
				} else {
					marqueurs.resultat = "Vous avez gagne !";
				}
			}

			if (joueur.figureJ1 === "CARRE" && joueur.figureJ2 === "CARRE") {
				if (joueur.moredicesJ1 > joueur.moredicesJ2) {
					marqueurs.resultat = "Vous avez gagné !";
					if (joueur.lancerJ2 >= 3) {}
				} else if (joueur.moredicesJ1 === joueur.moredicesJ2 && totalJ === totalIa) {
					marqueurs.resultat = "Egalite";
				} else {
					marqueurs.resultat = "Vous avez perdu";
				}
			}
			if (joueur.figureJ1 === "TRIPLE" && joueur.figureJ2 === "TRIPLE") {
				if (joueur.moredicesJ1 > joueur.moredicesJ2) {
					marqueurs.resultat = "Vous avez gagné !";
					if (joueur.lancerJ2 >= 3) {}
				} else if (joueur.moredicesJ1 === joueur.moredicesJ2 && totalJ === totalIa) {
					marqueurs.resultat = "Egalite";
				} else {
					marqueurs.resultat = "Vous avez perdu";
				}
			}

			if (joueur.figureJ1 === "CHANCE" && joueur.figureJ2 === "CHANCE") {
				if (totalJ > totalIa) {
					if (joueur.lancerJ2 >= 3) {
						marqueurs.resultat = "Vous avez gagné !";
					}
				} else if (totalJ === totalIa) {
					marqueurs.resultat = "Egalite";
				} else {
					marqueurs.resultat = "Vous avez perdu";
				}
			}

			if (joueur.figureJ1 === "YAMS") {
				if (joueur.figureJ2 === "FULL" || joueur.figureJ2 === "CARRE" || joueur.figureJ2 === "TRIPLE" || joueur.figureJ2 === "DOUBLE" || joueur.figureJ2 === "PETITE SUITE" || joueur.figureJ2 === "GRANDE SUITE" || joueur.figureJ2 === "CHANCE") {
					marqueurs.resultat = "Vous avez gagné !";
					if (joueur.lancerJ2 >= 3) {}
				}
			}
			if (joueur.figureJ1 === "GRANDE SUITE") {
				if (joueur.figureJ2 != "YAMS" && joueur.figureJ2 != "GRANDE SUITE" || joueur.figureJ2 === "CHANCE") {
					marqueurs.resultat = "Vous avez gagné !";
					if (joueur.lancerJ2 >= 3) {}
				}
			}
			if (joueur.figureJ1 === "PETITE SUITE") {
				if (joueur.figureJ2 === "FULL" || joueur.figureJ2 === "CARRE" || joueur.figureJ2 === "TRIPLE" || joueur.figureJ2 === "DOUBLE" || joueur.figureJ2 === "CHANCE") {
					marqueurs.resultat = "Vous avez gagné !";
					if (joueur.lancerJ2 >= 3) {}
				}
			}
			if (joueur.figureJ1 === "FULL") {
				if (joueur.figureJ2 === "CARRE" || joueur.figureJ2 === "TRIPLE" || joueur.figureJ2 === "DOUBLE" || joueur.figureJ2 === "CHANCE") {
					marqueurs.resultat = "Vous avez gagné !";
					if (joueur.lancerJ2 >= 3) {}
				}
			}
			if (joueur.figureJ1 === "CARRE") {
				if (joueur.figureJ2 === "TRIPLE" || joueur.figureJ2 === "DOUBLE" || joueur.figureJ2 === "CHANCE") {
					marqueurs.resultat = "Vous avez gagné !";
					if (joueur.lancerJ2 >= 3) {}
				}
			}
			if (joueur.figureJ1 === "TRIPLE") {
				if (joueur.figureJ2 === "DOUBLE" || joueur.figureJ2 === "CHANCE") {
					marqueurs.resultat = "Vous avez gagné !";
					if (joueur.lancerJ2 >= 3) {}
				}
			}

			if (joueur.lancerJ2 >= 3) {
				if (marqueurs.resultat != "Vous avez gagné !" && marqueurs.resultat != "Egalite") {
					joueursolo.dollar = Number(Number(joueursolo.dollar) - Number(50));
				} else {
					joueursolo.dollar = Number(Number(joueursolo.dollar) + Number(50));
				}
			}
			
			joueur.dollar = joueursolo.dollar;
			


			fs.writeFileSync(query.pseudo + "multi.json", JSON.stringify(joueur), "UTF-8");
			fs.writeFileSync(query.pseudo + ".json", JSON.stringify(joueursolo), "UTF-8");

			marqueurs.dollar = joueur.dollar;
			marqueurs.pseudo = query.pseudo;
			marqueurs.adv = connectes[i].adv;
			page = page.supplant(marqueurs);
		}
	}
	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write(page);
	res.end();
};

module.exports = trait;
